declare module 'meteor/msgfmt:core' {
	const mf: (key: string, params?: any, message?: any, locale?: any) => string;
	const mfPkg: any;
	const msgfmt: any;
}

declare const mfPkg: any;
